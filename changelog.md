## 1.0.0
ohos 第一个 release 版本
 * 实现了原库的大部分 api
 * 因为Animator目前XML方式只支持delay和duration属性，其他属性均需通过Java代码设置原因，animStyle() 没有实现
## 0.0.1-SNAPSHOT
ohos 第一个版本
 * 实现了原库的大部分 api
 * 因为Animator目前XML方式只支持delay和duration属性，其他属性均需通过Java代码设置原因，animStyle() 没有实现
