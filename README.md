# HiPermission

## 项目介绍
- 项目名称：HiPermission
- 所属系列：openharmony的第三方组件适配移植
- 功能：一个简单易用的漂亮权限申请库
- 项目移植状态：主功能完成（animStyle()尚未实现）
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release 1.0.7

## 效果演示
![](/screenshot/screenshot1.gif)

## 安装教程
1.在项目根目录下的build.gradle文件中，
```
// 添加maven仓库
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/release/'
    }
}
```

2.在entry模块的build.gradle文件中，
```
dependencies {
    implementation 'com.gitee.chinasoft_ohos:HiPermission:1.0.0'   
}
```


在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
一行代码搞定

``` java
HiPermission.create(context)
	.checkMutiPermission(new PermissionCallback() {
		@Override
		public void onClose() {
			Log.i(TAG, "onClose");
			showToast("用户关闭权限申请");
		}

		@Override
		public void onFinish() {
			showToast("所有权限申请完成");
		}

		@Override
		public void onDeny(String permission, int position) {
			Log.i(TAG, "onDeny");
		}

		@Override
		public void onGuarantee(String permission, int position) {
			Log.i(TAG, "onGuarantee");
		}
	});
```

就这样轻松搞定三个必备的权限

你想申请别的权限？那也没问题

``` java
List<PermissionItem> permissionItems = new ArrayList<PermissionItem>();
permissionItems.add(new PermissionItem(SystemPermission.CAMERA, "照相机", ResourceTable.Media_permission_ic_memory));
permissionItems.add(new PermissionItem(SystemPermission.LOCATION, "定位", ResourceTable.Media_permission_ic_location));
HiPermission.create(MainActivity.this)
			.permissions(permissionItems)
			.checkMutiPermission(...);
```

<big>**自定义主题**</big>

什么？想改下提示信息？界面不符合你的主题颜色？so easy

``` java
HiPermission.create(MainActivity.this)
			.title("亲爱的上帝")
			.permissions(permissionItems)
            .filterColor(getColor(ResourceTable.Color_colorPrimary))//图标的颜色
			.msg("为了保护世界的和平，开启这些权限吧！\n你我一起拯救世界！")
			.style(ResourceTable.Pattern_PermissionBlueStyle)
			.checkMutiPermission(...);
```

> 设置主题后一定要调用`filterColor()`,否者权限图标会变成默认的黑色


pattern.json

``` json
    {
      "name": "PermissionBlueStyle",
      "value": [
        {
          "name": "PermissionTitleColor",
          "value": "$color:colorPrimaryDark"
        },
        {
          "name": "PermissionMsgColor",
          "value": "$color:colorPrimary"
        },
        {
          "name": "PermissionItemTextColor",
          "value": "$color:colorPrimary"
        },
        {
          "name": "PermissionButtonBackground",
          "value": "$graphic:shape_btn"
        },
        {
          "name": "PermissionBackround",
          "value": "$graphic:shape_bg_white"
        },
        {
          "name": "PermissionButtonTextColor",
          "value": "$color:permissionColorWhite"
        }
      ]
    }
```
![](/screenshot/截图.PNG)

以下是每个属性的解释

| 属性名        | 类型           | 解释 |
| ------------- |:-------------:| -----:|
| PermissionTitleColor     | int | 标题文字颜色 |
| PermissionMsgColor| int | 描述文字颜色 |
| PermissionItemTextColor| int | 权限文字颜色 |
| PermissionButtonTextColor| int | 按钮文字颜色 |
| PermissionButtonBackground| drawable| 按钮背景 |
| PermissionBackround| drawable| 对话框背景 |
| PermissionBgFilterColor| int| 背景过滤色 |
| PermissionIconFilterColor| int|图标颜色 |

如果设置主题后不想调用`filterColor()`可在你的style里面添加`PermissionIconFilterColor`属性

<big>**默认图标**</big>
如果你需要申请其他权限，但是没有图标？`HiPermission`已经给你准备好啦~

| |日历|相机|联系人|定位|
|:-:|:-:|:-:|:-:|:-:|
| |![](/screenshot/permission_ic_calendar.png)|![](/screenshot/permission_ic_camera.png)|![](/screenshot/permission_ic_contacts.png)|![](/screenshot/permission_ic_location.png)|
|drawableId|permission_ic_calendar|permission_ic_camera|permission_ic_contacts|permission_ic_location|


| |麦克风|手机|短信|存储|传感器|
|:-:|:-:|:-:|:-:|:-:|:-:|
| |![](/screenshot/permission_ic_micro_phone.png)|![](/screenshot/permission_ic_phone.png)|![](/screenshot/permission_ic_sms.png)|![](/screenshot/permission_ic_storage.png)|![](/screenshot/permission_ic_sensors.png)|
|drawableId|permission_ic_micro_phone|permission_ic_phone|permission_ic_sms|permission_ic_storage|permission_ic_sensors|

使用图标

``` java
List<PermissionItem> permissions = new ArrayList<PermissionItem>();
//使用图标
permissions.add(new PermissionItem(SystemPermission.CAMERA, getString(ResourceTable.String_permission_cus_item_camera), ResourceTable.Media_permission_ic_camera));
HiPermission.create(MainActivity.this)
		.permissions(permissions)
		.style(ResourceTable.Pattern_PermissionDefaultGreenStyle)
		.checkMutiPermission(...);
```

>图标默认是黑色，需要调用`filterColor()`更改图标颜色

<big>**注意**</big>

> 清单文件一定要注册申请的权限，不然申请后会默认拒绝，而且设置-权限管理界面也无法看到此权限


## 测试信息
CodeCheck代码测试无异常  

CloudTest代码测试无异常  

病毒安全检测通过  

当前版本demo功能与原组件基本无差异  

## 版本迭代
- 1.0.0

## 版权和许可信息  
    Copyright (C) 2017 WeyYe

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.