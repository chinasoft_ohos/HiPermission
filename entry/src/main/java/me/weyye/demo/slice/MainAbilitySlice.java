/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package me.weyye.demo.slice;

import me.weyye.demo.ResourceTable;
import me.weyye.hipermission.HiPermission;
import me.weyye.hipermission.PermissionCallback;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.security.SystemPermission;

import java.util.Locale;

/**
 * 主界面
 *
 * @since 2021-04-12
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private static final long CLICK_INTERVAL = 1500L;

    private long lastClickTime;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getWindow().setStatusBarColor(getColor(ResourceTable.Color_colorPrimaryDark));
        Button btn1 = (Button) findComponentById(ResourceTable.Id_btn1);
        btn1.setText(btn1.getText().toUpperCase(Locale.ROOT));
        btn1.setClickedListener(this);
        Button btn2 = (Button) findComponentById(ResourceTable.Id_btn2);
        btn2.setText(btn2.getText().toUpperCase(Locale.ROOT));
        btn2.setClickedListener(this);
        Button btn3 = (Button) findComponentById(ResourceTable.Id_btn3);
        btn3.setText(btn3.getText().toUpperCase(Locale.ROOT));
        btn3.setClickedListener(this);
        Button btn4 = (Button) findComponentById(ResourceTable.Id_btn4);
        btn4.setText(btn4.getText().toUpperCase(Locale.ROOT));
        btn4.setClickedListener(this);
    }

    private void showToast(String text) {
        DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_layout_toast, null, false);
        Text msgToast = (Text) toastLayout.findComponentById(ResourceTable.Id_msg_toast);
        msgToast.setText(text);
        new ToastDialog(this)
                .setComponent(toastLayout)
                .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                .setAlignment(LayoutAlignment.BOTTOM)
                .setTransparent(true)
                .show();
    }

    /**
     * 防止多次点击
     *
     * @return 返回值
     */
    private boolean isFastClick() {
        long time = System.currentTimeMillis();
        if (time - lastClickTime < CLICK_INTERVAL) {
            return true;
        }
        lastClickTime = time;
        return false;
    }

    @Override
    public void onClick(Component component) {
        if (isFastClick()) {
            return;
        }
        switch (component.getId()) {
            case ResourceTable.Id_btn1:
                // default is normal style
                HiPermission.create(this)
                        .checkMutiPermission(new PermissionCallback() {
                            @Override
                            public void onClose() {
                                showToast(getString(ResourceTable.String_permission_on_close));
                            }

                            @Override
                            public void onFinish() {
                                showToast(getString(ResourceTable.String_permission_completed));
                            }

                            @Override
                            public void onDeny(String permission, int position) {
                            }

                            @Override
                            public void onGuarantee(String permission, int position) {
                            }
                        });
                break;
            case ResourceTable.Id_btn2:
                // After you have set the theme, you must called filterColor () to set the color of the icon
                // ,otherwise the default is black
                /* List<PermissionItem> permissionItems = new ArrayList<>();
                permissionItems.add(new PermissionItem(SystemPermission.GET_TELEPHONY_STATE,
                        "手机状态", me.weyye.hipermission.ResourceTable.Media_permission_ic_phone));
                HiPermission.create(this)
                        .title(getString(ResourceTable.String_permission_cus_title))
                        .permissions(permissionItems)
                        .msg(getString(ResourceTable.String_permission_cus_msg))
                        .style(me.weyye.hipermission.ResourceTable.Pattern_PermissionDefaultBlueStyle)
                        .checkMutiPermission(new PermissionCallback() {
                            @Override
                            public void onClose() {
                                showToast(getString(ResourceTable.String_permission_on_close));
                            }

                            @Override
                            public void onFinish() {
                                showToast(getString(ResourceTable.String_permission_completed));
                            }

                            @Override
                            public void onDeny(String permission, int position) {
                            }

                            @Override
                            public void onGuarantee(String permission, int position) {
                            }
                        });*/
                break;
            case ResourceTable.Id_btn3:
                /* List<PermissionItem> permissions = new ArrayList<PermissionItem>();
                permissions.add(new PermissionItem(SystemPermission.GET_TELEPHONY_STATE,
                        getString(ResourceTable.String_permission_cus_item_phone),
                        me.weyye.hipermission.ResourceTable.Media_permission_ic_phone));
                HiPermission.create(this)
                        .title(getString(ResourceTable.String_permission_cus_title))
                        .permissions(permissions)
                        .msg(getString(ResourceTable.String_permission_cus_msg))
                        .style(me.weyye.hipermission.ResourceTable.Pattern_PermissionDefaultGreenStyle)
                        .checkMutiPermission(new PermissionCallback() {
                            @Override
                            public void onClose() {
                                showToast(getString(ResourceTable.String_permission_on_close));
                            }

                            @Override
                            public void onFinish() {
                                showToast(getString(ResourceTable.String_permission_completed));
                            }

                            @Override
                            public void onDeny(String permission, int position) {
                            }

                            @Override
                            public void onGuarantee(String permission, int position) {
                            }
                        });*/
                break;
            case ResourceTable.Id_btn4:
                // request single permission only called onDeny or onGuarantee
                HiPermission.create(this).checkSinglePermission(SystemPermission.CAMERA, new PermissionCallback() {
                    @Override
                    public void onClose() {
                        showToast(getString(ResourceTable.String_permission_on_close));
                    }

                    @Override
                    public void onFinish() {
                        showToast(getString(ResourceTable.String_permission_completed));
                    }

                    @Override
                    public void onDeny(String permission, int position) {
                        showToast("onDeny");
                    }

                    @Override
                    public void onGuarantee(String permission, int position) {
                        showToast("onGuarantee");
                    }
                });
                break;
            default:
                break;
        }
    }
}
