package me.weyye.hipermission;

import static ohos.security.SystemPermission.CAMERA;
import static ohos.security.SystemPermission.LOCATION;
import static ohos.security.SystemPermission.WRITE_USER_STORAGE;

import me.weyye.hipermission.slice.PermissionAbilitySlice;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import ohos.bundle.IBundleManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Administrator on 2017/5/10 0010.
 */
public class HiPermission {
    private final Context mContext;

    private String mTitle;
    private String mMsg;
    private int mStyleResId = -1;
    private PermissionCallback mCallback;
    private List<PermissionItem> mCheckPermissions;
    private int mPermissionType;

    private String[] mNormalPermissionNames;
    private String[] mNormalPermissions = {
        WRITE_USER_STORAGE, LOCATION, CAMERA};
    private int[] mNormalPermissionIconRes = {
        ResourceTable.Media_permission_ic_storage,
        ResourceTable.Media_permission_ic_location,
        ResourceTable.Media_permission_ic_camera};
    private int mFilterColor = 0;
    private int mAnimStyleId = -1;

    public HiPermission(Context context) {
        mContext = context;
        mNormalPermissionNames = mContext.getStringArray(ResourceTable.Strarray_permissionNames);
    }

    public static HiPermission create(Context context) {
        return new HiPermission(context);
    }

    public HiPermission title(String title) {
        mTitle = title;
        return this;
    }

    public HiPermission msg(String msg) {
        mMsg = msg;
        return this;
    }

    public HiPermission permissions(List<PermissionItem> permissionItems) {
        mCheckPermissions = permissionItems;
        return this;
    }

    public HiPermission filterColor(int color) {
        mFilterColor = color;
        return this;
    }

    public HiPermission style(int styleResIdsId) {
        mStyleResId = styleResIdsId;
        return this;
    }

    private List<PermissionItem> getNormalPermissions() {
        List<PermissionItem> permissionItems = new ArrayList<>();
        for (int i = 0; i < mNormalPermissionNames.length; i++) {
            permissionItems.add(new PermissionItem(mNormalPermissions[i],
                mNormalPermissionNames[i], mNormalPermissionIconRes[i]));
        }
        return permissionItems;
    }

    public static boolean checkPermission(Context context, String permission) {
        int checkPermission = context.verifySelfPermission(permission);
        if (checkPermission == IBundleManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    /**
     * 检查多个权限
     *
     * @param callback
     */
    public void checkMutiPermission(PermissionCallback callback) {
        if (mCheckPermissions == null) {
            mCheckPermissions = new ArrayList<>();
            mCheckPermissions.addAll(getNormalPermissions());
        }

        // 检查权限，过滤已允许的权限
        Iterator<PermissionItem> iterator = mCheckPermissions.listIterator();
        while (iterator.hasNext()) {
            if (checkPermission(mContext, iterator.next().Permission)) {
                iterator.remove();
            }
        }
        mPermissionType = PermissionAbilitySlice.PERMISSION_TYPE_MUTI;
        mCallback = callback;
        if (mCheckPermissions.size() > 0) {
            startActivity();
        } else {
            if (callback != null) {
                callback.onFinish();
            }
        }
    }

    /**
     * 检查单个权限
     *
     * @param permission
     * @param callback
     */
    public void checkSinglePermission(String permission, PermissionCallback callback) {
        if (checkPermission(mContext, permission)) {
            if (callback != null) {
                callback.onGuarantee(permission, 0);
            }
            return;
        }
        if (!mContext.canRequestPermission(permission)) {
            if (callback != null) {
                callback.onDeny(permission, 0);
            }
            return;
        }
        mCallback = callback;
        mPermissionType = PermissionAbilitySlice.PERMISSION_TYPE_SINGLE;
        mCheckPermissions = new ArrayList<>();
        mCheckPermissions.add(new PermissionItem(permission));
        startActivity();
    }

    private void startActivity() {
        PermissionAbilitySlice.setCallBack(mCallback);
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
            .withBundleName(mContext.getBundleName())
            .withAbilityName(PermissionAbility.class.getName())
            .build();
        intent.setParam(ConstantValue.DATA_TITLE, mTitle);
        intent.setParam(ConstantValue.DATA_PERMISSION_TYPE, mPermissionType);
        intent.setParam(ConstantValue.DATA_MSG, mMsg);
        intent.setParam(ConstantValue.DATA_FILTER_COLOR, mFilterColor);
        intent.setParam(ConstantValue.DATA_STYLE_ID, mStyleResId);
        intent.setParam(ConstantValue.DATA_ANIM_STYLE, mAnimStyleId);
        intent.setParam(ConstantValue.DATA_PERMISSIONS, (Serializable) mCheckPermissions);
        intent.setOperation(operation);
        mContext.startAbility(intent, 0);
    }
}
