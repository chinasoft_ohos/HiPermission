/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package me.weyye.hipermission;

import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

/**
 * 自定义提示弹窗
 *
 * @since 2021-04-12
 */
public class AlertDialog extends CommonDialog {
    private Text tvTitle;
    private Text tvContent;
    private Text tvCancel;
    private Text tvSure;

    /**
     * AlertDialog
     *
     * @param context 上下文
     */
    public AlertDialog(Context context) {
        super(context);
        setTransparent(true);
        setAlignment(LayoutAlignment.CENTER);
        Component contentView = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_dialog_alert, null, true);
        setContentCustomComponent(contentView);
        initView();
        siteRemovable(false);
        siteKeyboardCallback((iDialog, keyEvent) -> true);
    }

    private void initView() {
        Component component = getContentCustomComponent();
        tvTitle = (Text) component.findComponentById(ResourceTable.Id_tvTitle);
        tvContent = (Text) component.findComponentById(ResourceTable.Id_tvContent);
        tvCancel = (Text) component.findComponentById(ResourceTable.Id_tvCancel);
        tvSure = (Text) component.findComponentById(ResourceTable.Id_tvSure);
    }

    /**
     * 设置标题
     *
     * @param title 标题
     */
    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    /**
     * 设置内容
     *
     * @param content 内容
     */
    public void setContent(String content) {
        tvContent.setText(content);
    }

    /**
     * 设置取消按钮文字和点击事件监听
     *
     * @param cancel 按钮文字
     * @param listener 点击事件监听
     */
    public void setCancel(String cancel, Component.ClickedListener listener) {
        tvCancel.setText(cancel);
        tvCancel.setClickedListener(listener);
    }

    /**
     * 设置确定按钮文字和点击事件监听
     *
     * @param sure 按钮文字
     * @param listener 点击事件监听
     */
    public void setSure(String sure, Component.ClickedListener listener) {
        tvSure.setText(sure);
        tvSure.setClickedListener(listener);
    }
}
